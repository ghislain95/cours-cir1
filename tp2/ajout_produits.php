<html lang="fr">

  <head>
    <link rel="stylesheet" href="ajoutproduit.css">
  <meta charset="utf-8">
  <title>Ajout Produit</title>

  </head>

  <body>
    <h1>Ajouter un produit</h1>

      <form action="./reponse_formulaire-file.php"
          method="POST" enctype="multipart/form-data">
        <fieldset id="identity">
            <p>
                <label for="firstname">Prénom</label></br>
                <input type="text" name="firstname" id="firstname"/>
            </p>
            <p>
                <label for="lastname">Nom</label></br>
                <input type="text" name="lastname" id="lastname"/>
            </p>
            <p>
                <label for="avatar">Votre photo</label></br>
                <input type="file" name="photo" id="photo"/>
            </p>
            <p>
                <label for="price">Prix</label></br>
                <input type="text" name="price" id="price"/>
                <?php if ($price < 0) {
                  echo "Le prix n'est pas valide";
                }?>
            </p>

            <?php
              define('TARGET_DIRECTORY', './');
              if(!empty($_FILES['avatar'])) {
              move_uploaded_file($_FILES['avatar']['tmp_name'], TARGET_DIRECTORY . $_FILES['avatar']['name']);
              }
              ?>
        </fieldset>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
</form>

  </form>
  </body>

</html>
